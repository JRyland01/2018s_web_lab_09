package ictgradschool.web.lab09.ex07;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.PrintWriter;

public class ImageGalleryDisplayServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServletContext servletContext = getServletContext();
        String fullPhotoPath = servletContext.getRealPath("Photos");

        File[] mainFiles = new File(fullPhotoPath).listFiles();
        int i = 0;
        while(i<mainFiles.length){
            String name =mainFiles[i].getName();
            if (name.endsWith("_thumbnail.png")){
                PrintWriter out = response.getWriter();
                response.setContentType("text/html");
                out.print("<img src=\""+name+"\"></img><br>");
                out.print("Name: "+name);
                out.print("Size: ");
            }

        }

        //    public ServletContext getServletContext()
//        return servletContext;
//    }

    }
}

