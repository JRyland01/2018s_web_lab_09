package ictgradschool.web.lab09.ex01;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ArticleServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String aTitle = request.getParameter("aTitle");
        String aAuthor = request.getParameter("aAuthor");
        String genre = request.getParameter("genre");
        String content = request.getParameter("content");

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<h1>"+aTitle+"</h1>");
        out.println("<p>By "+aAuthor+"<br>Genre:"+genre+"</p><br>");
        out.println(content);

        String[] myArray = new String[4];
        myArray[0] = "Four";
        myArray[1] = "Three";
        myArray[2] = "Two";
        myArray[3] = "One";
        out.println("<ul>");
        for (int i = 0; i<myArray.length; i++){
            out.print("<li>"+myArray[i]+"</li>");
        }
        out.println("</ul>");





    }
}
